const conn = require("../connecting");
const tag = require("./Tags");
const questionhs = require("./questionhs");
const validat = require("../Validate/validate");
const tagDl = require("../Delete/Tags");
const questionhsDl = require("../Delete/questionhs");
function Port(data, io) {
  const dataRT = validat(data);
  if (dataRT != null) {
    io.to(data.room).emit("msgErrorNewBlog", {
      check: false,
      value: dataRT,
    });
  } else {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    let date1=year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
    const questionhsData = data.question;
    const tagsData = data.tag;
    conn.connect(function () {
      var sql = `INSERT INTO datnfpoly.posts (user_id,type,anonymus,display_status,createAt) VALUES (${data.usersPost.id},${data.type},${data.anonymus},${data.display_status},'${date1}')`;
      conn.query(sql, function (err, results) {
        if (err) {
          io.to(data.room).emit("msgErrorNewBlog", {
            check: false,
            value: err,
          });
          console.log(err)
        } else {
          let idPosts = results.insertId;
          const dataQH = questionhs(questionhsData, idPosts,date1);
          const datatags = tag(tagsData, idPosts);
          //const dataUser = tag(data.usersPost, questionhsData.point,date1);
          if (dataQH.error != null || datatags.error != null) {
            //looi
            if (dataQH.error == null) {
              tagDl(idPosts);
            }
            if (datatags.error == null) {
              questionhsDl(idPosts);
            }
            io.to(data.room).emit("msgErrorNewBlog", {
              check: false,
              value: "lỗi tạo bảng",
            });
            console.log("lỗi tạo bảng")
          } else {
            io.to(data.room).emit("msgNewBlog",
            {
              id: results.insertId,
              type: data.type,
              anonymus: data.anonymus,
              display_status: data.display_status,
              question: {
                content: questionhsData.content,
                createDay: date1,
                id: questionhsData.id,
                img: questionhsData.img,
                point: questionhsData.point,
                title: questionhsData.title,
                updateDay: date1,
              },
              tag: tagsData,
              comment: 0,
              like: 0,
              usersPost: data.usersPost,
              disLike: 0,
              detailedComment: 0
            });
            console.log("gui thanh công")
         }
        }
      });
    });
  }
}
module.exports = Port;
