
const validate = (data)=>{
    let errors = {};
    for(var key in data){
        if(key.endsWith('text')){
            if (key.length < 4) {
                errors.text = 'bình luận của bạn không hợp lệ length 4';
            }
            else{
                //check lỗi ký tự nhập vào
               const value=checkText(data.text);
               if(value!=null){
                errors.text=value;
               }
            }
        }
    }
    if(Object.keys(errors).length>0){
        return errors;
    }
    else{
        return null;
    }
}
module.exports = validate;


let listletter =['ngô','anh'];
let listcharacters =[',','.'];
function checkText (data) {
    let errorsText = {};
    var str = data;
    var sts =str.split('');
    var stst =str.split(' ');
    //check ký tự không hợp lệ
    let charactersError =[];
    for(var st in sts){
        for(var list in listcharacters){
            if(listcharacters[list].endsWith(sts[st])){
                charactersError.push(sts[st]);
                break;
            }
        }
    } 
    if(charactersError.length>0){
        errorsText.charactersError =charactersError;
    }
   
    // check từ ngữ không hợp lệ
     let letterError =[];
     for(var st in stst){
        for(var list in listletter){
            if(listletter[list].endsWith(stst[st])){
                letterError.push(stst[st])
                break;
            }
        }
    } 

    if(letterError.length>0){
        errorsText.letterError =letterError;
    }
    if(Object.keys(errorsText).length>0){
        return errorsText;
    }
    else{
        return null;
    } 
}

