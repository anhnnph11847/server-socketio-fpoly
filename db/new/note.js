var mysql = require("mysql");
const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const tags = require("./db/Create/Tags");
const Relpy = require("./db/Create/Reply");
const Post = require("./db/Create/Port");
const questionhs = require("./db/Create/questionhs");
const Report = require("./db/Create/Report");
const validat = require("./db/Validate/validate");
const ReplySs = require("./db/Create/ReplySs");

const port = process.env.PORT || 9999;
const index = require("./index");
const app = express();
app.use(index);
const server = http.createServer(app);
const io = socketIo(server);
io.on("connection", (socket) => {
  socket.on("joinroom", (dl) => {
  //  console.log("joinroom" + dl);
    socket.join(dl, () => {
     /// console.log(socket.rooms);
    //console.log(io.sockets.adapter.rooms[1].length);
    });
  });
  //bình luận mới
  socket.on("Comment", (data) => {
    // var rooms = io.sockets.adapter.sids[socket.id]; 
    // for(var room in rooms) {       socket.leave(room);}
    // console.log("Comment")
    // socket.join(data.question_id)
    // Relpy(data,io);
    console.log("Comment" ,data)
    return ReplySs(data)
    .then(vl=>{
      if(vl.status!=1){
        console.log("loi",vl)
        io.to(data.question_id).emit("msgErrorComment", vl.error);
      }
      else{
        console.log("ddungs roi",vl)
        io.to(data.question_id).emit("msgComment",vl);
      }
      
    })
    .catch(loi=>{
      console.log("loi",loi)
      io.to(data.question_id).emit("msgErrorComment", loi);
    })
   
  //   io.to(data.question_id).emit("msgComment", 
  //   {
  //     id: 1000,
  //     user: {
  //       id: 1,
  //       fullname: "ngô ngọc anh",
  //       point: 200,
  //       role: "1",
  //       create_at: "2021-11-07T18:20:59.000+00:00",
  //       image: "img9.jpg"
  //     },
  //     content: "data.content1",
  //     type: 1,
  //     anonymus: 1,
  //     status: 1,
  //     img: null,
  //     createAt: "2021-12-12"
  //   });
  });
  
  //bài viết mới
   socket.on("NewBlog", (data) => {
    var rooms = io.sockets.adapter.sids[socket.id]; 
    for(var room in rooms) {       socket.leave(room);}
    console.log("newBlop")
    socket.join(data.room)
    Post(data,io);

      // io.to(data.room).emit("msgNewBlog",
      //       {
      //         id: 1000,
      //         type: data.type,
      //         anonymus: data.anonymus,
      //         display_status: data.display_status,
      //         question: data.question,
      //         tag: data.tag,
      //         comment: 0,
      //         like: 0,
      //         usersPost: data.usersPost,
      //         disLike: 0,
      //         detailedComment: 0
      //       });
   });
  //report
  socket.on("ReportPost", (data) => {
    console.log(data)
    Report(data,io);
   });

  socket.on("disconnect", () => {
    //console.log("Client disconnected");
  });
  

});
server.listen(port, () => console.log(`Listening on port ${port}`));
